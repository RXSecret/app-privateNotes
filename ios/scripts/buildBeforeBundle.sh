#!/bin/sh
#
# iOS build 之前 把RN程序编译好。 app运行 就是RN最新版
#
#  未完成，等待项目结尾时，再说吧！
#


RNPackageDebugSH=../script/iosDebug.sh
RNPackageReleaseSH=../script/iosRelease.sh


function package_with_sh() {

}

function package() {
    if [[ $CONFIGURATION == Debug* ]]; then
        package_with_sh $RNPackageDebugSH
    else
        package_with_sh $RNPackageReleaseSH
    fi
}

package
