//
//  main.m
//  privateNotes
//
//  Created by srxboys on 2019/4/15.
//  Copyright © 2019 srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
