/**
 * 
 * @project : privateNotes
 * 
 * @file: App.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Monday, April 15th 2019, 4:14:52 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Monday, April 15th 2019, 4:14:52 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * 
 */

'use strict'
import React, { Component } from 'react';
import { Text, TextInput } from 'react-native';
import PropTypes from 'prop-types'

import Home    from './app/screen/home/index'
import profile from './app/screen/profile/index'
import Secret  from './app/screen/secret/index'
import edit    from './app/screen/edit/index'


import { createStackNavigator } from "react-navigation"; 

const RootStack = createStackNavigator(
  { 
    Home: { screen: Home },
    Profile: {screen: profile},
    Secret: { screen: Secret },
    Edit: { screen: edit}
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
     //不跟随`系统字体大小`而变化
    TextInput.defaultProps = Object.assign({}, TextInput.defaultProps, {defaultProps: false});
    Text.defaultProps = Object.assign({}, Text.defaultProps, {allowFontScaling: false});

    return <RootStack />;
  }
}