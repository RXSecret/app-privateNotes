/**
 * 
 * @project : privateNotes
 * 
 * @file: index.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Monday, April 15th 2019, 4:18:03 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Sunday, April 28th 2019, 6:08:39 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `首页`
 * 
 *  @flow
 * 
 */

'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native'

// 第三方
import LinearGView from '../../components/Views/linearGradient'

// 内部控件
import NoteView    from './views/noteView'
import Menu        from '../../components/Views/menu'
import LinearGAdd      from '../../components/Views/add/linearGAdd'

// 工具
import RXPlatform  from '../../utils/RXPlatform'

//常亮
const ADD_WIDTH = RXPlatform.adaptationHeight(50);
const ADD_TOP = RXPlatform.height - RXPlatform.navigationHeight() - ADD_WIDTH - RXPlatform.ifIphoneX(50, 30, 30)


const width = RXPlatform.width;
const height = RXPlatform.height;
const navHeight = RXPlatform.navigationHeight();
const bottom = RXPlatform.ifIphoneX(20, 0, 0)

export default class Home extends Component {
  //导航栏配置
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('otherParam', '首页'),
      headerBackTitle: '返回'
    };
  };

  constructor(props) {
    super(props);
    this.state = ({

    })
  }

  render(){
    return(
      <View style={{flex: 1, backgroundColor: 'yellow'}}>
        <LinearGView 
          style={styles.container}
          orientation={'vertical'}
          subViews={this._contentView()}
        />
      </View>
    )
  }

  _contentView() {
    return(
      <View style={{flex: 1}}> 
      <ScrollView style={styles.scrollView}>
        <View style={styles.rightTopView}>
          <Menu width={20} height={10} style={{marginLeft: 10}} 
            onPress={()=>{
              
            }} />
        </View>

        <NoteView 
          {...this.props}
          style={styles.notes}
        />
      </ScrollView>
  
      <LinearGAdd 
        style={styles.add}
        diameter={ADD_WIDTH}
        borderStyle={{borderRadius: ADD_WIDTH/2}}
        onPress={()=>{
          this.props.navigation.push('Edit')
        }} 
      />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height,
    backgroundColor: 'black',
  },

  rightTopView: {
    marginTop: navHeight,
    left: width - 60,
    paddingRight: 20,
    width: 60,
  },

  scrollView: {
    flex: 1,
    paddingBottom: bottom,
  },

  notes: {

  },

  add: {
    position: 'absolute',
    top: ADD_TOP,
    left: width - 20 - ADD_WIDTH,
    width:ADD_WIDTH, 
    height:ADD_WIDTH, 
  },

})



