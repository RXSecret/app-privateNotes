/**
 * 
 * @project : privateNotes
 * 
 * @file: noteView.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 3:04:21 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 17th 2019, 3:04:21 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  VirtualizedList,
  DeviceEventEmitter
} from 'react-native'

import RXPlatform    from '../../../utils/RXPlatform'
import RXJSON        from '../../../utils/Cache/file/RXJSON'
import RXEmitterAPI  from '../../../utils/RXEmitterAPI'

import SubEmpty      from './subEmpty'
import SubNoteItem   from './subNoteItem'
import SubMyNoteView from './subMyNoteView'
import NodeHead      from './nodeHead'


const height = RXPlatform.height;
const width = RXPlatform.width;
const left = (RXPlatform.width - width)/2 
const itemWith = (width-20)/2;

export default class NoteView extends Component {
  /**
   * 首页-日记-布局主控件
   */

  static defaultProps = {
    style:{},
  }

  constructor(props) {
    super(props);
    this.state = ({
      models: []
    })
    
    var listener = null;
  }

  componentDidMount() {
    this._reloadData();
    this.listener = DeviceEventEmitter.addListener(RXEmitterAPI.RXEA_edit_success, (param)=>{
      this._reloadData();
    });
  }

  componentWillUnmount() {
    this.listener.remove();
  }

  _reloadData() {
    RXJSON.getTitlesInCache((flag, result)=>{
      if(flag) {
        if(Array.isArray(result)) {
          this.setState({models: result})
        }
      }
    });
  }
  
  render(){
    let models = this.state.models;
    return (
      <View style={[styles.container,this.props.style]}>
          <NodeHead {...this.props} style={styles.mynotes} />
          <FlatList 
            //加载数据源
            data={models}
            // extraData={models}
            //展示数据
            renderItem={({ index, item }) => 
                <SubNoteItem 
                  {...this.props}
                  key={index}
                  style={styles.item}
                  index={index} 
                  model={item} 
                />
            }

            //默认情况下每行都需要提供一个不重复的key属性
            keyExtractor={(item, index) => (index)}
            //设置垂直布局（）
            horizontal={false}
            //设置水平的排列的个数（只有垂直布局才起作用）
            numColumns={2}
            //行与行之间的分隔线组件
            ItemSeparatorComponent={this._separator} //这个必须是调用方法
            //如果设置了多列布局（即将numColumns 值设为大于1的整数），则可以额外指定此样式作用在每行容器上
            columnWrapperStyle={{paddingLeft:2}}

            //空列表的时候，view?
            ListEmptyComponent={ <SubEmpty {...this.props} />}
          />
      </View>
    )
  }

  _separator = () => {
    return <View style={{height:5}}/>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,
    // left,
    // width,
    paddingBottom: 20,
    borderRadius: 4,
  },

  mynotes: {
    height: 30,
    marginTop: 20,
  },

  item: {
    height: 50,
    marginLeft: 5,
    width: itemWith
  }
})

