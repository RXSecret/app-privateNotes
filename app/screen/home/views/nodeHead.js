/**
 * 
 * @project : privateNotes
 * 
 * @file: nodeHead.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Tuesday, April 23rd 2019, 10:51:12 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Tuesday, April 23rd 2019, 10:51:12 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native'

export default class NodeHead extends Component {
  static defaultProps = {
    style:{},
  }

  render(){
    return (
       <TouchableOpacity 
        style={this.props.style}
        onPress={()=>{
          // this.props.navigation.push('Secret')
        }}
       >
         <Text style={styles.text}>十二月</Text>
       </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginLeft: 20,
    marginRight:20,
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold',
    lineHeight: 20,
  },
})

