/**
 * 
 * @project : privateNotes
 * 
 * @file: noteItem.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 3:35:33 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 17th 2019, 3:35:33 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native'

export default class MyNoteView extends Component {
  static defaultProps = {
    style:{},

    index: 0,
    model:{}
  }

  render(){
    var newStyle = {
      flex:1,
      borderRadius: 4, 
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'rgb(98,90,200)'
    }

    let model = this.props.model || {};
    let id = model.id || 0
    let icon = model.icon || null;
    let title = model.title || ''
    let des = model.describe || ''
    let color = model.color || null
    let type = model.type || 0


    return (
       <TouchableOpacity 
        onPress={()=>{
          this.props.navigation.push('Edit', {id})
        }}
       >
       <View style={[newStyle,this.props.style]}>
          <Text  style={styles.text}>{title}</Text>
       </View>
       </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 14,
    // color: 'rgb(240,240,240)',
    color: 'rgb(200,200,200)',
    // color: 'white',
    lineHeight: 20,
    textAlign: 'center',
    // backgroundColor: 'blue'
  },
})

