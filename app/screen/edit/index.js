/**
 * 
 * @project : privateNotes
 * 
 * @file: index.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Friday, April 19th 2019, 6:01:45 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Sunday, April 28th 2019, 3:17:43 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback
} from 'react-native'

import RoundButton   from '../../components/Views/button/roundButton'

import SubEditHead   from './views/subEditHead'
import SubEdit       from './views/subEdit'

import RXPlatform    from '../../utils/RXPlatform'

export default class Home extends Component {
  //导航栏配置
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('otherParam', 'edit日记')
    };
  };

  constructor(props) {
    super(props);
    this.state = ({
      editing: false,
      valueLength: 0
    })
  }

  clearFocus() {
    if(this.refInput) {
      this.refInput.blur()
    }
  }

  render(){
    return(
      <TouchableWithoutFeedback
          style={{flex: 1}}
          onPress={()=>{
              this.clearFocus();
          }}
      >
      <View style={styles.container}>
        <SubEditHead 
          style={styles.headerView} 
          editing={this.state.editing}
          valueLength={this.state.valueLength}
        />
        <SubEdit 
          {...this.props}
          style={styles.fieldTextView}
          ref={refInput=>this.refInput=refInput}
          callBack={(editing, valueLength, title, content)=>{
            this.setState({editing, valueLength});
            title = title || '';
            content = content || '';
            this.title = title;
            this.content = content;
          }} 
         />
      </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },

  headerView: {

  },

  fieldTextView: {
    flex: 1,
  },
})



