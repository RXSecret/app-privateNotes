/**
 * 
 * @project : privateNotes
 * 
 * @file: iconBase.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Sunday, April 28th 2019, 7:26:05 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Monday, April 29th 2019, 3:28:22 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 * 
 * android 图片信息得知与图片位深度有关
 * https://blog.csdn.net/a845158277/article/details/84261777
 */

'use strict'

import React, { Component } from 'react';
import { StyleSheet } from 'react-native'

import Iconbutton from '../../../../components/Views/iconButton'

export default class IconBase extends Component {

  static defaultProps = {
    id: 0, //控制所有
    editing: false, //只对 save icon 有用
    enable: false, //只对 save icon 有用
  }

  constructor(props){
    super(props);
    this.state = ({
      isSelected: false,
      activeOpacity: false,
      isShow: false,
    })
  }
  
  getNomalIcon(){
    // 需要子类实现
    return null;
  }

  getSelectedIcon(){
    // 需要子类实现
    return null;
  }

  onClick() {
    // 需要子类实现
  }

  render(){
    let isSelected = this.state.isSelected;
    let icon = isSelected?this.getSelectedIcon():this.getNomalIcon();
    let isShow = this.state.isShow;
    return(
      <Iconbutton 
        {...this.props}
        onPress={()=>this.onClick()}
        style={styles.item}
        activeOpacity={this.state.activeOpacity}
        viewOpacity={isShow}
        imgSource={icon}
      />
    )
  }
}

const styles = StyleSheet.create({
  item: {
    height: 21,
    width: 21,
  }
})