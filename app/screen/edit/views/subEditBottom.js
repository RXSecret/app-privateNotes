/**
 * 
 * @project : privateNotes
 * 
 * @file: subEditBottom.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Friday, April 26th 2019, 5:01:45 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Monday, April 29th 2019, 3:28:54 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

 
'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  View
} from 'react-native'

import IconSave    from './iconViews/iconSave'
import IconDelete    from './iconViews/iconDelete'
import IconTop    from './iconViews/iconTop'
import IconLabel    from './iconViews/iconLabel'
import IconCollect    from './iconViews/iconCollect'

export default class SubEditBottom extends Component {
 
  static defaultProps = {
    style: {},

    id: 0,
    enable: false,
    editing: true, // 控制按钮是否可点击， 除了删除
  }

  render(){
    return(
      <View style={[this.props.style, styles.container]}>   
        <IconSave {...this.props} />
        <IconDelete {...this.props} />
        <IconTop {...this.props} />
        <IconLabel {...this.props} />
        <IconCollect {...this.props} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: 21,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: 'row',
  },

  item: {
    flex: 1,
    height: 21,
    width: 21,
  }
})


