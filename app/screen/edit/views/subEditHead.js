/**
 * 
 * @project : privateNotes
 * 
 * @file: subEditHead.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Saturday, April 20th 2019, 3:53:53 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Sunday, April 28th 2019, 3:28:30 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native'

// 第三方
import LinearGView from '../../../components/Views/linearGradient'

import DateView from '../../../components/Views/calendar/date'
import WeekView from '../../../components/Views/calendar/week'

export default class Home extends Component {
 
  static defaultProps = {
    style: {},
    editing: false,
    valueLength: 0,
  }

  constructor(props) {
    super(props);
    this.state = ({

    })
  }

  render(){
    return(
      <View style={this.props.style}>
        <LinearGView 
          style={styles.container}
          orientation={'vertical'}
          subViews={this._contentView()}
        />
      </View>
    )
  }

  _contentView() {
    let valueLength = this.props.valueLength || 0;
    let editing = this.props.editing || false;
    return (
      <View style={styles.content} >
        <View style={styles.date}>
          <DateView style={[styles.dateItem, styles.dateItemLeft]} />
          <WeekView style={[styles.dateItem, styles.dateItemRight]} />
        </View>
        <View style={styles.date}>
          <Text style={[styles.count, styles.dateItemLeft]} numberOfLines={1}>{editing?'未保存':(valueLength<0?'已保存':'未编辑')}</Text>
          <Text style={[styles.count, styles.dateItemRight]} numberOfLines={1}>{'字数: '+valueLength}</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 6,
    backgroundColor: 'white',
    padding: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },

  content: {

  },

  date: {
    flexDirection: 'row',
  },

  dateItem: {
    flex: 1,
  },

  dateItemLeft: {
    textAlign: 'left'
  },

  dateItemRight: {
    textAlign: 'right'
  },

  count: {
    flex: 1,
    marginTop: 5,
    color: 'white',
    textAlign: 'right'
  }
})