/**
 * 
 * @project : privateNotes
 * 
 * @file: RXClick.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Monday, April 15th 2019, 4:24:29 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Monday, April 15th 2019, 4:24:29 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `防止重复点击`
 * 
 * @flow
 */




'use strict'

var lastClickTime = 0;

const ClickUtil = {

  /**
   * @this 事件点击 => 防止重复点击 (根据情况斟酌)
   *
   */
  canClick: function(){
    var now = new Date().getTime();
    if (now - lastClickTime>500) {
        lastClickTime = now;
        return true;
    } else {
        console.log(lastClickTime);
        return false;
    }
  }
}
export default ClickUtil;