/**
 * 
 * @project : privateNotes
 * 
 * @file: RXJSON.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 7:06:18 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Monday, April 29th 2019, 2:58:28 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */


'use strict'
import RXFileSystem from './RXFileSystem'
import RXString     from '../../RXString'

const FILE_NAME_edit_path = 'rx_pn_edit/'
const FILE_NAME_path = FILE_NAME_edit_path+'notes'
const FILE_NAME_titles = FILE_NAME_edit_path+'rx_pn_edit_titles.json'


class RXJSON extends RXFileSystem {

  static NOTE_TITLE_KEY =  'pn_note_title_key'
  static NOTE_CONTENT_KEY =  'pn_note_content_key'

  /**
   * 
   * @param {*} callback 
   */
  static getTitlesInCache(callback=null){
    callback = callback || null;
    this.readFile(FILE_NAME_titles, null, null, (flag, obj)=>{
      var result = null;
      if(flag) {
        let dictJson = obj;
        if(!dictJson || dictJson.length < 1) return null;
        let dict = JSON.parse(dictJson);
        var array = []
        for (var key in dict) { 
          let newDict = 
          {
            'id': key || '',
            'title': dict[key] || 'null'
          }
          array.push(newDict)
        }
        result = array;
      }
      
      if(callback) {
        callback(flag, result)
      }
    });
  }

  /**
   * 
   */
  static getFileContentInCache(id=0, callback){
    if(id < 1) {
      if(callback) {
        callback(false, null);
      }
      return;
    }
    let path = FILE_NAME_path+'/'+ id +'.json';
    this.readFile(path, null, null, (flag, result)=>{
      if(!flag) return;
      if(!result) return;
      if(typeof result === 'string') {
        result = JSON.parse(result);
      }
      if(callback) {
        callback(result);
      }
    });
  }

  /**
   * 
   * @param {*} title 
   * @param {*} content 
   * @param {*} id 
   * @param {*} callback 
   */
  static save(title='', content='', id=0, callback=null) {
    title = title || ''
    content = content || ''
    id = id || 0;

    if(!id) {
      id = (new Date()).getTime() + '';
    }
    
    let path = FILE_NAME_path+'/'+id+'.json';
    let titleKey = this.NOTE_TITLE_KEY;
    let contentKey = this.NOTE_CONTENT_KEY;

    var dict = {};
    dict[titleKey] = title;
    dict[contentKey] = content;

    let dictJson = JSON.stringify(dict)

    this.writeFile(path, dictJson, null, null, 
      (flag)=>{
        if(flag) {
          this.synchronize(title, id, callback);
        }
        else {
          if(callback) {
            callback(false, null);
          }
        }
    });
  }


  /**
   * 
   * @param {*} title 
   * @param {*} id 
   * @param {*} callback 
   */
  static synchronize(title, id, callback) {
    var dictJson = this.readFile(FILE_NAME_titles, null, null,
       (flag, obj)=>{

        var dictJson = JSON.stringify({});
        if(flag) {
          dictJson = obj || JSON.stringify({});
        }
  
        var dict = JSON.parse(dictJson);
        dict[id] = title;
        dictJson = JSON.stringify(dict)
        this.writeFile(FILE_NAME_titles, dictJson, null, null,
          (flag)=>{
            callback(flag, id)
        });
      }
    )
   
  }

}
export default RXJSON;





