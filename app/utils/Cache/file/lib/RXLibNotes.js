/**
 * 
 * @project : privateNotes
 * 
 * @file: RXLibNotes.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Thursday, April 25th 2019, 10:55:25 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Saturday, April 27th 2019, 8:57:15 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `日记 - 增删改查`
 * 
 * @flow
 * 
 * 
 *  根据 `id` 存、删 
 * 
 * 未完成
 */


'use strict'
import RXFileSystem from '../RXFileSystem'
import RXCacheKey   from '../RXCacheKey'


const NOTES_KEY_id      = 'id';      //唯一标识 - 创建时间-时间戳(Date)
const NOTES_KEY_mtime   = 'mtime';   //修改时间-时间戳(Date)
const NOTES_KEY_title   = 'title';   //标题(string)
const NOTES_KEY_content = 'content'; //内容(string)
const NOTES_KEY_views   = 'views';   //浏览次数(int)
const NOTES_KEY_top     = 'top';     //制定(bool)
const NOTES_KEY_collect = 'collect'; //收藏(bool)
const NOTES_KEY_labels  = 'labels';  //标签(string)
const NOTES_KEY_path    = 'path';    //路径(string) 不能修改

export default class RXLibNotes extends RXFileSystem {

  
  /**
   * 根据`id`获取详细内容信息
   * 
   * @param {*} id       唯一标识(创建时间Date)
   * @param {*} callback 回调
   */
  static getFileContentInCache(id=0, callback){
    if(id < 1) {
      callback(false, null);
      return;
    }

    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let day = date.getDate();
    var relative = year+'/'+month+'/'+day+'/'+id+'.json';
    let path = RXCacheKey.Lib_notes+relative;

    this.readFile(path, null, null, (flag, result)=>{
      if(!flag) return;
      if(!result) return;
      if(typeof result === 'string') {
        result = JSON.parse(result);
      }

      let views = dict[NOTES_KEY_views];
      dict[NOTES_KEY_views]= views+1;

      //id.json  和 文件内容字段id 是否认为损坏、破坏文件，
      //还是矫正下？

      if(callback) {
        callback(result);
      }

    });
  }

  /**
   *  日记 - 保存处理
   * 
   * @param {*} id       唯一标识(创建时间Date)
   * @param {*} title    标题(string)
   * @param {*} content  内容(string)
   * @param {*} top      置顶(bool)
   * @param {*} collect  收藏(bool)
   * @param {*} labels   标签(array<string>)
   * @param {*} callback 回调
   */
  static save(id=0, title='', content='', top=false, views=0, collect=false, labels=[], callback=null) {
    id = id || 0;
    title = title || ''
    content = content || ''
    top = top || false
    views = views || 0
    collect = collect || false
    labels = labels || []

    if(!id) {
      id = (new Date()).getTime() + '';
    }

    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let day = date.getDate();
    var relative = year+'/'+month+'/'+day+'/'+id+'.json';

    let path = RXCacheKey.Lib_notes+relative;
    let mtime = (new Date()).getTime() + '';

    var dict = {};
    dict[NOTES_KEY_id]      = id
    dict[NOTES_KEY_mtime]   = mtime;
    dict[NOTES_KEY_title]   = title;
    dict[NOTES_KEY_content] = content;
    dict[NOTES_KEY_views]   = views;
    dict[NOTES_KEY_top]     = top;
    dict[NOTES_KEY_collect] = collect;
    dict[NOTES_KEY_labels]  = labels;
    dict[NOTES_KEY_path]    = path;

    let dictJson = JSON.stringify(dict)

    this.writeFile(path, dictJson, null, null, 
      (flag)=>{
        if(flag) {
          this.synchronize(title, id, callback);
        }
        else {
          if(callback) {
            callback(false, null);
          }
        }
    });
  }

  /**
   * 日记 - 删除文件  (未完成)
   * 
   * @param {*} id 
   * @param {*} callback 
   */
  static delete(id=0, callback=nul){
    if(!id || (typeof id !== Number) || id <= 0) {
      if(callback) {
        console.log('')
        callback(false, null);
      }
      return;
    }

    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let day = date.getDate();
    var relative = year+'/'+month+'/'+day+'/'+id+'.json';

    let path = RXCacheKey.Lib_notes+relative;

    this.deleteFile(path, null, (flag, isFilepath, result)=>{
      if(flag) {
        // this.synchronize(title, id, callback);
      }
      if(callback) {
        callback(flag);
      }
    })
  }

  /**
   * 
   * 未完成 - 存、删 后的同步到 收藏、标签、置顶
   * 
   * @param {*} title 
   * @param {*} id 
   * @param {*} callback 
   */
  static synchronize(title, id, callback) {
    // var dictJson = this.readFile(FILE_NAME_titles, null, null,
    //    (flag, obj)=>{

    //     var dictJson = JSON.stringify({});
    //     if(flag) {
    //       dictJson = obj || JSON.stringify({});
    //     }
  
    //     var dict = JSON.parse(dictJson);
    //     dict[id] = title;
    //     dictJson = JSON.stringify(dict)
    //     this.writeFile(FILE_NAME_titles, dictJson, null, null,
    //       (flag)=>{
    //         callback(flag, id)
    //     });
    //   }
    // )
   
  }

  getAllYears() {
    this.readDir(RXCacheKey.Lib_notes, null, 
      (flag, isFilepath, result)=>{
        console.log('result=\n'+result)
      });
  }

  getAllMonths(year=0){
    
  }

  getAllDays(year=0, month=0){
    
  }

}




