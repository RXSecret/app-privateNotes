/**
 * 
 * @project : privateNotes
 * 
 * @file: RXLibCollects.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Thursday, April 25th 2019, 1:24:35 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Thursday, April 25th 2019, 1:24:35 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `收藏 - 增删改查`
 * 
 * @flow
 * 
 * 
 * 根据 `path` 存、删
 * 
 */


'use strict'
import RXFileSystem from '../RXFileSystem'
import RXCacheKey   from '../RXCacheKey'


export default class RXLibCollects extends RXFileSystem {

  static COLLECT_KEY_ctmie  = 'ctmie';  //唯一标识 - 创建时间-时间戳(Date)
  static COLLECT_KEY_mtmie  = 'mtmie';  //唯一标识 - 修改时间-时间戳(Date)
  static COLLECT_KEY_paths  = 'paths';  //路径(string)


  
  /**
   * 获取所有 `Collect` 收藏
   * 
   * @param {*} callback 
   */
  static getRemindsInCache(callback){
    this.readFile(RXCacheKey.Lib_reminds_json, null, null, (flag, result)=>{
      if(!flag) {
        if(callback) {
          callback(false, null);
        }
        return;
      }
      if(!result) {
        if(callback) {
          callback(false, null);
        }
        return;
      }
      if(typeof result === 'string') {
        result = JSON.parse(result);
      }

      if(callback) {
        callback(true, result);
      }
    });
  }
   
  /**
   * 加入 `收藏`
   * @param {*} path 
   * @param {*} callback 
   */
  static save(path='', callback=null) {
    this._libCollectOption(true, path, callback);
  }

  /**
   * 加入 `收藏`
   * @param {*} path 
   * @param {*} callback 
   */
  static delete(path=null, callback=null) {
    this._libCollectOption(false, path, callback); 
  }

  static _libCollectOption(_isSave=true, path=null, callback=null) {
    _isSave = _isSave?true:false;
    path = path || 0;
    if((typeof path !== 'string') || !path.length) {
      console.warn('RXLibCollects '+_isSave?'save':'delete'+'() path==null');
      callback(false, null);
    }

    let ctmie = (new Date()).getTime() + '';

    this.getRemindsInCache((flag, result)=> {
      var rootDict = {}
      if(flag) {
        rootDict = result;
      }

      if(!rootDict.length) {
        rootDict[COLLECT_KEY_ctmie] = ctmie;
      }

      rootDict[COLLECT_KEY_mtmie] = ctmie;

      let current_dict = rootDict[COLLECT_KEY_paths];
      if(_isSave) {
        //新增
        current_dict[path] = null;
      }
      else {
        //删除
        delete current_dict[path];
      }
      rootDict[COLLECT_KEY_paths] = current_dict;
      
      let dictJson = JSON.stringify(rootDict);

      this.writeFile(RXCacheKey.Lib_collects_json, dictJson, null, null, 
        (flag)=>{
          if(flag) {
            if(callback) {
              callback(true, rootDict);
            }
          }
          else {
            if(callback) {
              callback(false, null);
            }
          }
      });

    });  
  }
}

