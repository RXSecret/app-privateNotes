/**
 * 
 * @project : privateNotes
 * 
 * @file: RXLibTops.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Thursday, April 25th 2019, 4:23:20 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Thursday, April 25th 2019, 4:23:20 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `置顶 增删改查`
 * 
 * @flow
 * 
 * 根据 `文件路径` 存、删
 * 
 */


'use strict'
import RXFileSystem from '../RXFileSystem'
import RXCacheKey   from '../RXCacheKey'


export default class RXLibTops extends RXFileSystem {

  static TOP_KEY_ctmie  = 'ctmie';  //唯一标识 - 创建时间-时间戳(Date)
  static TOP_KEY_mtmie  = 'mtmie';  //唯一标识 - 修改时间-时间戳(Date)
  static TOP_KEY_paths  = 'paths';  //路径(string)


  
  /**
   * 获取所有 `tops` 置顶的路径
   * 
   * @param {*} callback 
   */
  static getTopsInCache(callback){
    this.readFile(RXCacheKey.Lib_tops_json, null, null, (flag, result)=>{
      if(!flag) {
        if(callback) {
          callback(false, null);
        }
        return;
      }
      if(!result) {
        if(callback) {
          callback(false, null);
        }
        return;
      }
      if(typeof result === 'string') {
        result = JSON.parse(result);
      }

      if(callback) {
        callback(true, result);
      }
    });
  }

  /**
   *  删除 置顶
   * 
   * @param {*} path      将要保存的路径
   * @param {*} callback 
   */
  static delete(path='', callback=null) {
    this._libTopOption(false ,path, callback);
  }

  static save(path='', callback=null) {
    this._libTopOption(true, path, callback);
  }
   
  /**
   *  加入置顶
   * 
   * @param {*} path      将要保存的路径
   * @param {*} callback 
   */
  static _libTopOption(_isSave=true, path='', callback=null) {
    _isSave = _isSave?true:false
    path = path || 0;
    if((typeof path !== 'string') || !path.length) {
      console.warn('RXLibCollects save() path==null');
      callback(false);
    }

    let ctmie = (new Date()).getTime() + '';

    this.getRemindsInCache((flag, result)=> {
      var rootDict = {}
      if(flag) {
        rootDict = result;
      }
      else {
        if(!_isSave) {
          if(callback) {
            callback(true, null)
          }
          return;
        }
      }

      if(!rootDict.length) {
        rootDict[TOP_KEY_ctmie] = ctmie;
      }

      rootDict[TOP_KEY_mtmie] = ctmie;

      var current_dict = rootDict[TOP_KEY_paths];
      if(current_dict) {
        if(_isSave) {
          current_dict[path] = null;
        }
        else {
          delete current_dict[path];
        }
      }
      else {
        if(_isSave) {
          current_dict = {}
          current_dict[path] = null;
        }
        else {
          callback(true, null)
          return;
        }
      }
      
      rootDict[TOP_KEY_paths] = current_dict;
      
      let dictJson = JSON.stringify(rootDict);

      this.writeFile(RXCacheKey.Lib_tops_json, dictJson, null, null, 
        (flag)=>{
          if(flag) {
            if(callback) {
              callback(true, rootDict);
            }
          }
          else {
            if(callback) {
              callback(false, null);
            }
          }
      });

    });  
  }
}
