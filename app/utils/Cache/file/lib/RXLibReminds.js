/**
 * 
 * @project : privateNotes
 * 
 * @file: RXLibReminds.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Thursday, April 25th 2019, 10:56:04 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Thursday, April 25th 2019, 10:56:04 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `提醒 - 增删改查`
 * 
 * @flow
 * 
 * 
 * 根据 `id` 存、删
 * 
 */

'use strict'
import RXFileSystem from '../RXFileSystem'
import RXCacheKey   from '../RXCacheKey'


export default class RXLibReminds extends RXFileSystem {

  static REMIND_KEY_id           = 'id';    //唯一标识 - 创建时间-时间戳(Date)
  static REMIND_KEY_title        = 'title';  //内容
  static REMIND_KEY_startDate    = 'startDate'; //开始日期(Date)
  static REMIND_KEY_endDate      = 'endDate'; //结束日期(Date)

  
  /**
   * 获取所有 `label` 标签
   * 
   * @param {*} callback 
   */
  static getRemindsInCache(callback){
    this.readFile(RXCacheKey.Lib_reminds_json, null, null, (flag, result)=>{
      if(!flag) {
        if(callback) {
          callback(false, null);
        }
        return;
      }
      if(!result) {
        if(callback) {
          callback(false, null);
        }
        return;
      }
      if(typeof result === 'string') {
        result = JSON.parse(result);
      }

      if(callback) {
        callback(true, result);
      }
    });
  }

  static save(id=0, title='', startDate=0, endDate=0, callback=null) {
    this._libRemindOption(true, id, title, startDate, endDate, callback);
  }

  static delete(id=0, callback=null) {
    this._libRemindOption(false, id, callback);
  }
   
  /**
   * 
   * 提醒 - 保存处理
   * 
   * @param {*} id        唯一标识(创建时间Date)
   * @param {*} title     标题/内容
   * @param {*} startDate 开始时间(目前是保留字段，不启作用)
   * @param {*} endDate   结束时间(目前是保留字段，不启作用)
   * @param {*} callback 
   */ 
  static _libRemindOption(_isSave=true,id=0, title='', startDate=0, endDate=0, callback=null) {
    _isSave = _isSave?true:false;
    id = id || 0;
    title = title || '';
    startDate = startDate || 0;
    endDate = endDate || 0;

    if(!id) {
      if(!_isSave) {
        console.warn('RXLibReminds delete() id=null');
        return;
      }
      id = (new Date()).getTime() + '';
    }

    var dict = {};
    dict[this.REMIND_KEY_id]        = id
    dict[this.REMIND_KEY_title]     = title;
    dict[this.REMIND_KEY_startDate] = startDate;
    dict[this.REMIND_KEY_endDate]   = endDate;

    this.getRemindsInCache((flag, result)=> {
      var rootDict = {}
      if(flag) {
        rootDict = result;
      }
      else {
        if(callback) {
          callback(true, null);
        }
        return;
      }

      if(_isSave) {
        rootDict[id] = dict;
      }
      else {
        delete rootDict[id];
      }
      
      let dictJson = JSON.stringify(rootDict);

      this.writeFile(RXCacheKey.Lib_reminds_json, dictJson, null, null, 
        (flag)=>{
          if(flag) {
            if(callback) {
              callback(true, rootDict);
            }
          }
          else {
            if(callback) {
              callback(false, null);
            }
          }
      });

    });  
  }

}



