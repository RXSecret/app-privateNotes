/**
 * 
 * @project : privateNotes
 * 
 * @file: RXLibLabels.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Thursday, April 25th 2019, 10:55:07 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Thursday, April 25th 2019, 10:55:07 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `标签 - 增删改查`
 * 
 * @flow
 * 
 * 
 * 根据`text` 存、删
 * 
 */

'use strict'
import RXFileSystem from '../RXFileSystem'
import RXCacheKey   from '../RXCacheKey'


export default class RXLibLabels extends RXFileSystem {

static LABELS_KEY_text         = 'text';  //标签 内容
static LABELS_KEY_ctmie        = 'ctmie';    //唯一标识 - 创建时间-时间戳(Date)
static LABELS_KEY_mtmie        = 'mtmie';    //唯一标识 - 修改时间-时间戳(Date)
static LABELS_KEY_times        = 'times'; //次数(int)
static LABELS_KEY_paths        = 'paths';  //路径(string)
static LABELS_KEY_color        = 'color'; //字颜色(string)
static LABELS_KEY_bgColor      = 'bgColor'; //背景颜色(string)
static LABELS_KEY_borderEnable = 'borderEnable'; //边框是否可用(string)
static LABELS_KEY_borderColor  = 'borderColor';  //边框颜色(string)

static LABELS_OPT_KEY_SAVE = 'save';    //保留一个新的路径到 某标签缓存
static LABELS_OPT_KEY_DEL  = 'delete';  //删除 某标签 所在的路径
static LABELS_OPT_KEY_DEL_LABEL = 'delete_label'; //删除 某标签 以及 内部所有路径

  
  /**
   * 获取所有 `label` 标签
   * 
   * @param {*} callback 
   */
  static getLabelsInCache(callback){
    this.readFile(RXCacheKey.Lib_labels_json, null, null, (flag, result)=>{
      if(!flag) {
        if(callback) {
          callback(false, null);
        }
        return;
      }
      if(!result) {
        if(callback) {
          callback(false, null);
        }
        return;
      }
      if(typeof result === 'string') {
        result = JSON.parse(result);
      }

      if(callback) {
        callback(true, result);
      }
    });
  }
   

  /**
    * `label` 标签 - 保存处理
    * @param {*} path           路径
    * @param {*} text           标签文本
    * @param {*} ctime          唯一标识(创建时间Date)
    * @param {*} times          次数
    * @param {*} color          字颜色
    * @param {*} bgColor        背景颜色
    * @param {*} borderEnable   边框 是否可用
    * @param {*} borderColor    边框 颜色
    * @param {*} callback       回调
    */
  static save(text='', path=null, ctime=0, times=1, color='', bgColor='', borderEnable=false, borderColor='', callback=null) {
    this._libLabelOption(this.LABELS_OPT_KEY_SAVE, text, path, ctime, color, bgColor, borderEnable, borderColor, callback);
  }


  /**
   * `label` 标签 - 删除路径
   * 
   * @param {*} text 
   * @param {*} path 
   * @param {*} callback 
   */
  static delete(text, path, callback) {
    this._libLabelOption(this.LABELS_OPT_KEY_DEL, text, path, null, null, null, null, null, callback);
  }


  static deleteLabel(text, callback) {
    this._libLabelOption(this.LABELS_OPT_KEY_DEL_LABEL, text, path, null, null, null, null, null, callback);
  }

  
   static _libLabelOption(_type='', text='', path=null, ctime=0, times=1, color='', bgColor='', borderEnable=false, borderColor='', callback=null) {
    _type = _type || this.LABELS_OPT_KEY_SAVE;
    text = text || '';
    path = path || null;
    ctime = ctime || 0;
    times = times || 1;
    color = color || '';
    bgColor = bgColor || '';
    borderEnable = borderEnable || false;
    borderColor = borderColor || '';

    if(!path) {
      console.warn('RXLibLabels save() path==null');
      callback(false, null);
      return;
    }

    if(!ctime) {
      ctime = (new Date()).getTime() + '';
    }

    var dict = {};
    dict[this.LABELS_KEY_ctime]        = ctime;
    dict[this.LABELS_KEY_mtime]        = (new Date()).getTime() + ''
    dict[this.LABELS_KEY_times]        = times;
    dict[this.LABELS_KEY_color]        = color;
    dict[this.LABELS_KEY_bgColor]      = bgColor;
    dict[this.LABELS_KEY_borderEnable] = borderEnable;
    dict[this.LABELS_KEY_borderColor]  = borderColor;

    this.getLabelsInCache((flag, result)=> {
      var rootDict = {}
      if(flag) {
        rootDict = result;
      }
      else {
        if(_type === this.LABELS_OPT_KEY_DEL_LABEL) {
          //删除这个标签
          if(callback) {
            callback(true, null);
          }
          return;
        }
      }

      var current_text_dict = rootDict[text]
      if(!current_text_dict) { //就没有这个 标签的key
        if(_type === this.LABELS_OPT_KEY_DEL || _type === this.LABELS_OPT_KEY_DEL_LABEL) { 
          //删除操作
          if(callback) {
            callback(true, null)
          }
          return;
        }
        //新增操作
        dict[this.LABELS_KEY_paths] = {path : null}
        rootDict[text] = dict;
      }
      else {
        if(_type === this.LABELS_OPT_KEY_DEL_LABEL) {
          //删除这个标签
          delete rootDict[text];
        }
        else {
          var paths = current_text_dict[this.LABELS_KEY_paths];
          if(paths) {
            if(!_type) {
              //删除操作
              delete paths[path];
            }
            else {
              //新增操作
              paths[path] = null;
            }
            dict[this.LABELS_KEY_paths] = paths;
          }
          else {
            if(_type) {
              //新增操作
              dict[this.LABELS_KEY_paths] = {path : null}
            }
          }
          rootDict[text] = dict;
        }
      }
      
      let dictJson = JSON.stringify(rootDict);

      this.writeFile(RXCacheKey.Lib_labels_json, dictJson, null, null, 
        (flag)=>{
          if(flag) {
            if(callback) {
              callback(true, rootDict);
            }
          }
          else {
            if(callback) {
              callback(false, null);
            }
          }
      });

    });  
  }

}

