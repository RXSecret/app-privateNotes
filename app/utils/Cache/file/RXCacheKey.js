/**
 * 
 * @project : privateNotes
 * 
 * @file: RXCacheKey.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Sunday, April 21st 2019, 9:04:22 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Sunday, April 21st 2019, 9:23:59 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */




'use strict'



class RXCacheKey {


  /**
   *   存储主目录
   */
  static ROOT_path = 'privateNotes';

  /**
   *   存储方式
   */
  static TYPE_lib = 'store_type_lib';
  static TYPE_cache = 'store_type_cache';


  static Lib_notes = 'notes';

  static Lib_noteTitles = 'noteTitles';



  
  static Lib_labels_json = 'labels.json';
  static Lib_reminds_json = 'reminds.json';
  static Lib_collects_json = 'collects.json';
  static Lib_tops_json = 'tops.json';

}
export default RXCacheKey;
