/**
 * 
 * @project : privateNotes
 * 
 * @file: RXStorage.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 6:09:21 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 17th 2019, 6:09:21 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `AsyncStorage 数据持久化`
 * 
 * @flow
 * 
 * https://www.jianshu.com/p/b394eefce7f0
 */


'use strict'

import { AsyncStorage } from 'react-native'

class RXStorage {

  /**
     * 根据key获取json数值
     * @param key
     * @returns {Promise<TResult>}
     */
    static get(key) {
      return AsyncStorage.getItem(key).then((value) => {
          if (value && value != '') {
              const jsonValue = JSON.parse(value);
              return jsonValue;
          } else {
              return null
          }
      }).catch(() => {
          return null
      });
  }

  /**
   * 保存key对应的json数值
   * @param key
   * @param value
   * @returns {Promise<string>}
   */
  static save(key, value) {
      return AsyncStorage.setItem(key, JSON.stringify(value));
  }

  /**
   * 更新key对应的json数值
   * @param key
   * @param value
   * @returns {Promise<TResult>|Promise.<TResult>|Promise<T>}
   */
  static update(key, value) {
      return AsyncStorage.get(key).then((item) => {
          value = typeof value === 'string' ? value : Object.assign({}, item, value);
          return AsyncStorage.setItem(key, JSON.stringify(value));
      });
  }

  /**
   * 删除key对应json数值
   * @param key
   * @returns {Promise<string>}
   */
  static delete(key) {
      return AsyncStorage.removeItem(key);
  }

  /**
   * 删除所有配置数据
   * @returns {Promise<string>}
   */
  static clear() {
      return AsyncStorage.clear();
  }

  /*
  e.g::::
    async getStorage() {
        let logic = await super.get(logic);
        if (!logic) {
            // 如果是首次登录， 跳转到引导界面
            name = 'Guide';
            component = Guide
        } else {
            if (!logic.isGuide) {
                // 如果没有引导
                name = 'Guide';
                component = Guide;
            } else if (!logic.isLogin) {
                // 如果没有登录
                name = 'Login';
                component = Login;
            } else {
                name = 'Main';
                component = Main;
            }
        }
        this.setState({
            name: name,
            component: component,
            params: params,
            modalVisible: false
        });
        this.timer && clearTimeout(this.timer);
    }
  */

}
export default RXStorage;


