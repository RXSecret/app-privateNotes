/**
 * 
 * @project : privateNotes
 * 
 * @file: RXRequest.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Monday, April 15th 2019, 6:41:34 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Monday, April 15th 2019, 6:41:34 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `请求类`
 * 
 * @flow
 * 
 */

'use strict'

const RXRequest = {

  /**
   *  POST 请求
   * 
   * @param {*} url 
   * @param {*} params  (new FormData())
   * 
      let params = new FormData()
      params.append('sort', '男')
      params.append('format', 'json')
   */
  post: function(url='', params=null){
    return this.request('POST', url, params)
    .then((result)=>{
      if (typeof(result)==="string") {
        // for android
        //如果存在超大整型的数值，JSON.parse 会失真( > 9007199254740992)
        console.warn('string\n'+result)
        result = JSON.parse(result);
      }
      else {
        console.warn('啥啥\n'+result)
      }
      //TODO add code handler here
      
      return result;
    });
  },

  /**
   * GET 请求
   * 
   * @param {*} url 
   * @param {*} params 
   */
  get: function(url=''){
    return this.request('GET', url, null)
    .then((result)=>{
      if (typeof(result)==="string") {
        // for android
        //如果存在超大整型的数值，JSON.parse 会失真( > 9007199254740992)
        result = JSON.parse(result);
      }
      //TODO add code handler here
      return result;
    });
  },

  request: function(type='POST', url='', params=null) {

    let init = {
      method: type,
      credentials: 'include',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'text/html, application/json, text/plain'
      },
      body: params?params:null
    };

    return new Promise(function (resolve, reject) {
      fetch(url, init)
      .then(response => response.text())
      .then(data => {
          resolve(data);
      }).catch(function (ex) {
          reject(ex);
          console.warn(' xxxxxx ')
      });
    })
  },

  /**
   * upload 上传
   * 
   * @param {*} url 
   * @param {*} params   (new FormData())
   * 
      let params = new FormData()
      params.append('sort', '男')
      params.append('format', 'json')
   */
  upload: function(url, params) {
    let init = {
      method: 'POST',
      credentials: 'include',
      headers: {
          'Content-Type': 'multipart/form-data',
      },
      body: params
    };
    return new Promise(function (resolve, reject) {
        fetch(url, init)
        .then(response => response.json())
        .then(data => {   
            resolve(data);
        }).catch(function (ex) {
            reject(ex);
        });
    });
  },
}

export default RXRequest;



/**
    // let url = 'http://baike.baidu.com/api/openapi/BaikeLemmaCardApi?scope=103&format=json&appid=379020&bk_key=关键字&bk_length=600'
    // RXRequest.get(url+'&format=json')


    let url = 'https://api.uomg.com/api/rand.avatar';

    /// this is 【 error 】 write ==> come_from: "https://blog.csdn.net/codetomylaw/article/details/52588493"
    // let params = {
    //   sort:'男',
    //   format:'json'
    // }

    /// this is 【 right 】 write
    let params = new FormData()
    params.append('sort', '男')
    params.append('format', 'json')

    
    RXRequest.post(url, params)
    .then((result) => {
      console.warn(JSON.stringify(result));
    })
    .catch((e) => {
      alert(e)
    });

    
 */