/**
 * 
 * @project : privateNotes
 * 
 * @file: RXString.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Monday, April 15th 2019, 4:23:44 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Monday, April 15th 2019, 4:23:44 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `RXString 字符串的处理`
 * 
 * @flow
 */

'use strict'

const RXString = {
  /**
   * @fileoverview  可用的字符串
   * 
   * @param {*} time 
   */
  isString: function(_string) {
    if(!_string || (typeof _string) !== 'string' || _string.length < 1)  {
      return false;
    }
    return true;
  },

  /**
   * 字符串 替换 
   * 
   * @param {*} strObj              源 - 字符串
   * @param {*} pos                 开始位置
   * @param {*} replaceText         将要替换的文本
   * @param {*} originTextLength    源 - 开始位置 的长度 是将要被替换的
   */
  replacePos(strObj, pos, replaceText, originTextLength=0) {
    var str = ''
    if(pos <= 0) {
        var str = replaceText+strObj
    }
    else if(pos < strObj.length-1 ) {
        if(originTextLength<=0) {
          console.warn('error  replacePos=> originTextLength==0');
          return '';
        }
        str = strObj.substr(0, pos-1) + replaceText + strObj.substring(pos+originTextLength, strObj.length);
    }
    else if(pos >= strObj.length-1 ) {
        str = strObj + replaceText;
    }
    return str;
  },
}

export default RXString;
