/**
 * 
 * @project : privateNotes
 * 
 * @file: RXPlatform.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Monday, April 15th 2019, 4:24:04 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Monday, April 15th 2019, 4:24:04 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `针对各个平台的 屏幕大小、样式`
 * 
 *  @flow
 * 
 */

'use strict'

import {
  Dimensions,
  Platform,
  PixelRatio
} from 'react-native'

const X_WIDTH = 375;
const X_HEIGHT = 812;
const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;
// const PAD_WIDTH = 768;
// const PAD_HEIGHT = 1024;


const { height, width} = Dimensions.get('window');
const DeviceWidth = width;
const DeviceHeight = height;

const size = {width, height}

const RXPlatform = {

  /** screen.size */
  size,

  /** screen.width */
  width,

  /** screen.height */
  height,

  // statusHeight=(this.ifIphoneX(44, 20, 20)),
  // navigationHeight=this.ifIphoneX(88, 64, 64),
  navigationHeight: function() {
    return this.ifIphoneX(88, 64, 64)
  },

  adaptationHeight: function(_height) {
    if(this.isIphone) {
      return _height*DeviceWidth/375
    }
    return PixelRatio.getPixelSizeForLayoutSize(_height)
  },

  adaptationWidth: function(_width) {
    if(this.isIphone) {
      return _width*DeviceHeight/667
    }
    return PixelRatio.getPixelSizeForLayoutSize(_width)
  },

  /** */
  isIphone: function() {
    return (
      Platform.OS === 'ios'
    )
  },

  /** iPhoneX */
  isIphoneX: function(){
    if (Platform.OS === 'web') return false; 
 
    // console single
    // // DeviceOrientationPortrait  && (ios => DeviceOrientationPortraitUpsideDown)
    // let iphonex_Portrait = (DeviceHeight === X_HEIGHT && DeviceWidth === X_WIDTH); 
    // // OrientationLandscapeLeft
    // let iphonex_LandscapeLeft_right = (DeviceHeight === X_WIDTH && DeviceWidth === X_HEIGHT);
    // let iphoneXSMax_Portrait = (DeviceHeight === XSMAX_HEIGHT && DeviceWidth === XSMAX_WIDTH);
    // let iphoneXSMax_LandscapeLeft_right = (DeviceHeight === XSMAX_WIDTH && DeviceWidth === XSMAX_HEIGHT);

    return (
      Platform.OS === 'ios' &&
      //Portrait && (ios => PortraitUpsideDown)  人像
      ((DeviceHeight === X_HEIGHT && DeviceWidth === X_WIDTH) || 
      // OrientationLandscapeLeft OrientationLandscapeRight  风景
      (DeviceHeight === X_WIDTH && DeviceWidth === X_HEIGHT)) ||

      ((DeviceHeight === XSMAX_HEIGHT && DeviceWidth === XSMAX_WIDTH) ||
      (DeviceHeight === XSMAX_WIDTH && DeviceWidth === XSMAX_HEIGHT))
    );
  },

  isAndroid: function(){
    return  this.isIphone()?false:true;
  },


  /**
   * components style in any platform
   * @param {*} iphoneXStyle  {}
   * @param {*} iphoneStyle   {}
   * @param {*} androidStyle  {}
   */
  ifIphoneX:function(iphoneXStyle={}, iphoneStyle={}, androidStyle={}) {
    if(this.isIphoneX()) {
      return iphoneXStyle;
    }
    if(this.isIphone()) {
      return iphoneStyle;
    }
    return androidStyle;
  },


  ifIphone:function(iphoneStyle={}, androidStyle={}) {
    return this.ifIphoneX(iphoneStyle, iphoneStyle, androidStyle);
  },

}

export default RXPlatform;


