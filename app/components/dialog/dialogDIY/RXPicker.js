/**
 * 
 * @project : privateNotes
 * 
 * @file: RXPicker.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Friday, April 12th 2019, 11:17:46 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Friday, April 12th 2019, 11:17:46 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

'use strict'
import React, { Component } from 'react'
import {
	StyleSheet,
	Text,
	View,
	Animated,
	TouchableWithoutFeedback
} from 'react-native'

import FadeAnimation from 'react-native-rxdialog/src/animations/FadeAnimation'
import SlideAnimation from 'react-native-rxdialog/src/animations/SlideAnimation'

import {DeviceWidth, DeviceHeight} from 'react-native-rxdialog/src/util/PlatformType.js'

const width = DeviceWidth;
const height = DeviceHeight;

export default class RXPicker extends Component {
    constructor(props){
      super(props);

      this.overlayAnimal = new FadeAnimation();
      this.pickerAnimal = new SlideAnimation({slideFrom : 'bottom'});
    }

    static defaultProps = {
      visible: false,
      style: {},
      superCallBack : {}
    }

    componentWillReceiveProps(nextProps) {
      const { visible } = this.props;
      if (visible !== nextProps.visible && nextProps.visible) {
        this._doAnimal(1, ()=>{
        });
      }
    }

    createContentView(){
      // 需要子类去实现
      console.warn('not implemented yet');
      return(
        <TouchableWithoutFeedback
              onPress={()=>{
                this._superCallBack(-1);
              }
            }>
          <View style={{flex: 1, width: width- 40, height: 80, alignItems: 'center', justifyContent: 'center', marginBottom: 50, backgroundColor: 'white'}}>
            <Text style={{padding:10, fontSize: 16, lineHeight: 20, color: 'red'}}>Override this method with subclasses to implement Component(请用子类覆盖该方法实现UI)</Text>
          </View>
        </TouchableWithoutFeedback>
      )
    }

    _superCallBack(action=0) {
      if(action != 0) {
        this._doAnimal(0, ()=>{
          if(this.props.superCallBack) {
            this.props.superCallBack(action);
          } 
        })
      }
    }

    _doAnimal(toValue: number, onFinished?: Function = () => { }) {
      this.overlayAnimal.toValue(toValue);
      this.pickerAnimal.toValue(toValue, onFinished )
    }

    _overlay(){
        return(
          <Animated.View style={[styles.overlay, this.overlayAnimal.animations]}>
            <TouchableWithoutFeedback
              onPress={()=>{
                this._superCallBack(-1);
              }
            }>
              <View style={styles.overlay} />
            </TouchableWithoutFeedback>
          </Animated.View>
        )
    }

    _content(){
      return(
        <Animated.View style={[styles.dialog, this.pickerAnimal.animations]}>
          {this.createContentView()}
        </Animated.View>
      )
    }


    render() {
      if(this.props.visible) {
        return ( 
          <View style={[styles.container, styles.dialog]}>
            {this._overlay()}
            {this._content()}
          </View>
        )
      }
      return null;
    }
}



const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      alignItems: 'center',
      position: 'absolute',
      justifyContent: 'flex-end',
      overflow: 'visible', // hidden  visible scroll
    },
  
    overlay: {
      ...StyleSheet.absoluteFillObject,
      position: 'absolute',
      width,
      height,
      backgroundColor: 'rgba(0,0,0,0.3)',
    },
  
    dialog: {
      overflow: 'visible', // hidden  visible scroll
      position: 'absolute',
    },
  
  })
