/**
 * 
 * @project : privateNotes
 * 
 * @file: RXToash.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Friday, April 12th 2019, 11:17:46 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Friday, April 12th 2019, 11:17:46 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 * -------------------------------------------
 * 
 * 可调用的方法列表:
 * @function hiddenAll   隐藏所有
 * @function show        显示(子类实现 【必须】)
 * @function hide        隐藏
**/

'use strict'
import React from 'react'
import {
  Text,
  View,
  ActivityIndicator
} from 'react-native'

import Dialog         from 'react-native-rxdialog/src/main/Dialog'
import DialogTopView  from 'react-native-rxdialog/src/level/DialogTopView'

const borderRadius = 4;

export default class RXToash extends Dialog {

  /**
   * @param {*} message       内容
   * @param {*} messageOption 内容样式
   * @param {*} showLoading   显示，加载动画，这个就只能手动移除了
   * @param {*} cacelAction   点击取消
   */
  static show(message, messageOption, showLoading, cancelAction) {
    message = message || '';
    messageOption = messageOption || {};
    showLoading = showLoading || false
    cancelAction = cancelAction || null;

    if (message.length < 1 ) {
      console.log('message 不能同时为空', message);
      return;
    }

    let key;
    const toashView = (
       <View style={{borderRadius, backgroundColor: 'rgba(0,0,0,.6)', overflow: 'hidden', justifyContent: 'center'}}>
          {
            showLoading?
            <View>
              <Text 
              onPress={()=>{
                DialogTopView.remove(key)
                if(cancelAction) {
                  cancelAction();
                }
              }}
              style={{marginTop: 1, width: 60,backgroundColor: 'red', textAlign: 'center', color: 'white'}}> 关闭 </Text>
              <ActivityIndicator size="small" color="#00ff00" style={{marginTop: 20}} />
            </View>
            : null
          }
          <Text style={[{padding: 10,fontSize: 14, color: 'white', lineHeight: 25, textAlign: 'center'},
           {...messageOption.messageTextStyle}]}>{'  '+ message + '   '}</Text>
       </View>
    )
    let element = Dialog.addPropsValue(this, toashView, 'toash', (index)=>{});
    key = DialogTopView.add(element);

    if(!showLoading) {
      setTimeout(
        () => {
          DialogTopView.remove(key)
       },
      2000
      )
    }

    return key;
  }


  // 遮罩层 样式
  static getOverlayStyles() {
    return {
      backgroundColor: 'rgba(0,0,0,0.0)'
    }
  }
}