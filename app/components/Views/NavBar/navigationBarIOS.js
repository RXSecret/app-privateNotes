/**
 * 
 * @project : privateNotes
 * 
 * @file: navigationBar.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 10:45:08 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 17th 2019, 10:45:08 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */


'use strict'

import React, { Component } from 'react';

import {
 StyleSheet,
 Text,
 View,
 Image,
 NativeModules,
 TouchableOpacity
} from 'react-native'

const NAV_LEFT_ICON = require('../../assets/comment/navi_bar_back.png')
export default class BaseIndex extends Component {

  static defaultProps = {
    title:'',
    left : '返回',
    right: '',
  }

  goBack(){
    let  navigation = this.props.navigation;
    if(navigation) {
      navigation.goBack();
    } 
  }


  _left() {
    if(this.props.onLeftPress){
      return(
          <TouchableOpacity onPress={()=>{
            this.goBack()
          }}>
            <View style={[style.DefaultItem, style.Item_left1]} >
              <Image style={style.Image}
                source={NAV_LEFT_ICON} />
              <Text style={style.TextLeft_1} numberOfLines={1} allowFontScaling={false} >{this.props.left}</Text>
            </View>
          </TouchableOpacity>
      )
    }
    else {
      return( <View style={[style.DefaultItem, style.Item_left1]} /> )
    }
  }

  _middle() {
    return(
      <View style={style.DefaultTitle}>
        <Text style={style.TextTitle} numberOfLines={1} allowFontScaling={false} >{this.props.title}</Text>
      </View>
    )
  }

  _right() {
    if(this.props.onRightPress) {
      return(
          <TouchableOpacity onPress={this.props.onRightPress}>
            <View style={[style.DefaultItem, style.Item_Right1]}>
              <Text style={style.TextRight_1} numberOfLines={1} allowFontScaling={false}>
                {this.props.right}
              </Text>
            </View>
          </TouchableOpacity>
      )
    }
    else {
      return(
          <View style={[style.DefaultItem, style.Item_Right1]}>
            <Text style={style.TextRight_1} numberOfLines={1} allowFontScaling={false} >
              {this.props.right}
            </Text>
          </View>
    )
    }
  }


  _lineView() {
    return(
      <View style={style.Line} />
    )
  }

  _getView() {

    return(
      <View style={style.Container}>
          <View style={style.Content}>
            {this._left()}
            {this._middle()}
            {this._right()}
          </View>
          {this._lineView()}
      </View>
    )
  }

  render(){
    return(
      <View>
        {this._getView()}
      </View>
    )
  }
}



const style = StyleSheet.create({

  Container: {
    width: DeviceWidth,
    height: AllHeight,
    backgroundColor: 'white',
  },

  Content: {
    width: DeviceWidth,
    height: ContentHeight,
    paddingTop: statusHeight,
    flexDirection: 'row',
    alignItems: 'center',
        // test code
        // backgroundColor: 'yellow',
  },

  DefaultItem: {
    width:70,
    height:20,
    flexDirection:'row',
    alignItems:'center',
    justifyContent: 'center',
        // test code
        // backgroundColor:'coral',
  },

  DefaultTitle: {
    flex:1,
    marginLeft:0,
    height:20,
    alignItems:'center',
    justifyContent: 'center',
    flexWrap:'wrap',
        // test code
        // backgroundColor:'cyan',
  },

  Item_left1: {
    paddingLeft:15,
    marginRight:5,
  },

  Item_Right1: {
    paddingRight:20,
    marginLeft:5,
  },

  TextLeft_1 : {
      flex:1,
      paddingLeft:5,
      fontSize:16,
      color:'#4e82d7',
      height:18,
      flexWrap:'wrap',
  },

  TextTitle : {
      flex:1,
      fontSize:18,
      fontWeight:'bold',
      color:'#2e353b',
      height:20,
  },

  TextRight_1 : {
      flex:1,
      fontSize:16,
      color:'#4e82d7',
      height:18,
      justifyContent: 'center',
      textAlign : 'right',
      flexWrap:'wrap',
  },

  Image : {
    width:12,
    height:20,
  },

  Line : {
    height:LineHeight,
    backgroundColor:'lightgray',
  }
});

