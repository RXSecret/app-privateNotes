/**
 * 
 * @project : privateNotes
 * 
 * @file: linearGradient.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Tuesday, April 16th 2019, 5:51:26 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Tuesday, April 16th 2019, 5:51:26 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `渐变View
 * 
 * @flow
 * 
 */

'use strict'


import React, { Component } from 'react';

import * as PropTypes from "prop-types";

import LinearGradient from 'react-native-linear-gradient'

const orientation_horizontal = 'horizontal'
const orientation_vertical = 'vertical'
const orientation_leftTopRightBottom = 'left_top_right_bottom' 
const orientation_RightTopLeftBottom = 'right_top_left_bottom'

export default class linearGradient extends Component {
  static defaultProps = {
    style:{flex: 1},
    subViews:null,
    orientation: orientation_horizontal,

    // colors: ['#FC8936', '#FCC636'], // 黄色渐变
    // colors: ['#93D7F0', '#93D7F0'], // 浅蓝渐变
    
    colors: ['rgb(26,101,255)', 'rgb(98,180,251)'], //蓝色渐变
  }

  render(){
    let colors = this.props.colors;

    var start_point = {x: 0,y: 0};
    var end_point = {x: 1,y: 0};

    let orientation = this.props.orientation;
    if(orientation === orientation_vertical) {
      start_point = {x: 1,y: 0};
      end_point = {x: 1,y: 1};
    }
    else if(orientation === orientation_leftTopRightBottom) {
      start_point = {x: 0,y: 0};
      end_point = {x: 1,y: 1};
    }
    else if(orientation === orientation_RightTopLeftBottom) {
      start_point = {x: 1,y: 0};
      end_point = {x: 0,y: 1};
    }
    return(
        <LinearGradient 
            style={this.props.style}
            colors={colors}
            start={start_point}
            end={end_point}
          >
          {this.props.subViews}
        </LinearGradient>
    )
  }
}







