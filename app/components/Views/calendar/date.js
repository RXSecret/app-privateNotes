/**
 * 
 * @project : privateNotes
 * 
 * @file: date.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Saturday, April 20th 2019, 9:26:55 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Saturday, April 20th 2019, 9:26:55 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native'

import RXDate from '../../../utils/RXDate'

export default class DateView extends Component {
  static defaultProps = {
    style: {},
    dateTime:0,
  }

  render(){
    let dateTime = this.props.dateTime || new Date().getTime();
    let dateString = RXDate.FormatString(dateTime, 'YY-MM-DD');
    return(
      <Text style={[styles.container, this.props.style]}>{dateString}</Text>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    color: 'white',
  },

})