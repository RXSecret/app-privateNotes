/**
 * 
 * @project : privateNotes
 * 
 * @file: iconButton.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Friday, April 26th 2019, 5:03:01 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Monday, April 29th 2019, 3:25:35 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `icon 按钮`
 * 
 * @flow
 * 
 */
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity
} from 'react-native'

export default class IconButton extends Component {
  static defaultProps = {
    style: {},
    activeOpacity: false,
    viewOpacity: true,
    imgSource: null, //require('xx/xx.png') 、{uri:'http://xx.png'}
  }

  render(){
    let activeOpacity = this.props.activeOpacity;
    let viewOpacity = this.props.viewOpacity;
    return (  
        <TouchableOpacity
          style={[styles.container, this.props.style]}
          activeOpacity={activeOpacity?0.4:1}
          onPress={()=>{
            if(!viewOpacity) return;
            if(this.props.onPress) {
              this.props.onPress();
            }
          }}
        >
          <Image source={this.props.imgSource} style={[styles.img, {opacity:viewOpacity?1:0}]} />
        </TouchableOpacity>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  
  img: {
    width: 21,
    height: 21,
  }

})
