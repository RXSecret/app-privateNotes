/**
 * 
 * @project : privateNotes
 * 
 * @file: roundButton.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Saturday, April 20th 2019, 3:32:41 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Saturday, April 20th 2019, 3:32:41 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */


'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native'

import LinearGradient from '../linearGradient'
import ARTRoundButton from '../../art/art-round-button'

export default class RoundButton extends Component {
  static defaultProps = {
    style: {},
    opacity: false,
    diameter: 50,
    textStyle: {},
    borderStyle: {},
  }

  render(){
    var opacity = this.props.opacity;
    if(!this.props.onPress) {
      opacity = false;
    }
    
    return (  
        <TouchableOpacity
          activeOpacity={opacity?0.7:1}
          onPress={()=>{
            if(this.props.onPress) {
              this.props.onPress();
            }
          }}
          style={[{height: 50},this.props.style]}
        >
          <LinearGradient 
            style={[styles.container, this.props.borderStyle]}
            colors={this.props.colors}
            subViews={this._button()}
          /> 
        </TouchableOpacity>   
    )
  }

  _button() {
    return(
      <ARTRoundButton diameter={this.props.diameter}/>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

})




