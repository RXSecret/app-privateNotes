/**
 * 
 * @project : privateNotes
 * 
 * @file: add.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Friday, April 19th 2019, 5:02:05 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Friday, April 19th 2019, 5:02:05 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

'use strict'

import React, { Component } from 'react';
import {
  TouchableOpacity
} from 'react-native'

import ARTAdd from '../../art/art-add'

export default class Add extends Component {
  static defaultProps = {
    style: { height: 10 },
    opacity: true,
  }

  render(){
    return (  
        <TouchableOpacity 
          style={{flex:1}}
          activeOpacity={this.props.opacity?0.7:1}
          onPress={()=>{
            if(this.props.onPress) {
              this.props.onPress();
            }
          }}
        >
        <ARTAdd {...this.props} />
        </TouchableOpacity>
    )
  }
}

