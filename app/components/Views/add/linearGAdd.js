/**
 * 
 * @project : privateNotes
 * 
 * @file: linearGAdd.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Friday, April 19th 2019, 5:27:21 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Friday, April 19th 2019, 5:27:21 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */




'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native'

import LinearGradient from '../linearGradient'
import ARTAdd         from '../../art/art-add'

export default class LinearGAdd extends Component {
  static defaultProps = {
    style: { height: 40, width: 40 },
    opacity: false,
    diameter: 40,
    addStyle: {},
    borderStyle: {},
    colors: ['#FC8936', '#FCC636'], //
    addSpace: 10,
  }

  render(){
    let opacity = this.props.opacity;
    if(opacity) {
      if(!this.props.onPress) {
        opacity = false;
      }
    }

    return (  
        <TouchableOpacity
          activeOpacity={opacity?0.7:1}
          onPress={()=>{
            if(this.props.onPress) {
              this.props.onPress();
            }
          }}
          style={[{height: 44},this.props.style]}
        >
          <LinearGradient 
            style={[styles.container, this.props.borderStyle]}
            colors={this.props.colors}
            subViews={this._add()}
          /> 
        </TouchableOpacity>   
    )
  }

  _add() {
    return(
      <ARTAdd style={[styles.add,this.props.textStyle]} 
       diameter={this.props.diameter} 
       space={this.props.addSpace} 
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  add: {
   
  },
})

