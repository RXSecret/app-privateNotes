/**
 * 
 * @project : privateNotes
 * 
 * @file: art-home.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 11:10:22 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 17th 2019, 11:10:22 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 * 
 *        不可用
 */


'use strict'
import React, { Component } from 'react'
import {
  StyleSheet,
  Dimensions,
  ART
} from 'react-native'

var {
  Surface,  //  一个矩形可渲染的区域，是其他元素的容器
  Group,    // 可容纳多个形状、文本和其他的分组
  Shape,    // 形状定义，可填充
  Path,     // 路径
  LinearGradient,    // 渐变色
  Pattern,           // 填充图片
  ClippingRectangle, // 剪辑
} = ART;

const DeviceWidth = Dimensions.get('window').width;      //设备的宽度
const DeviceHeight = Dimensions.get('window').height;    //设备的高度

export default class ARTHome extends Component {
  static defaultProps = {
    style: {},
    width: 0,
    height: 0,
    verticalLine: 8,
    baseLine: 16,
    fillColor: '#6E9EEC'
  }

  

  render() {
    let width = this.props.width;
    let height = this.props.height;

    let baseLine = this.props.baseLine;
    let verticalLine = this.props.verticalLine;

    let middenWidth = (width-baseLine)/2;

    let space = 1;

    let triangle_path = Path()
      .moveTo(middenWidth+baseLine, height+space)
      .lineTo(width/2, height+verticalLine)
      .lineTo(middenWidth, height+space);

    return(
      <Surface style={[{backgroundColor: 'white'},this.props.style]} width={width} height={height+verticalLine}>
        <Shape d={triangle_path} stroke={this.props.fillColor}/>
      </Surface>
    )
  }
}


