/**
 * 
 * @project : privateNotes
 * 
 * @file: art-round-button.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Saturday, April 20th 2019, 3:20:48 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Saturday, April 20th 2019, 3:20:48 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */


'use strict'
import React, { Component } from 'react'
import {
  Dimensions,
  ART
} from 'react-native'

var {
  Surface,  //  一个矩形可渲染的区域，是其他元素的容器
  Group,    // 可容纳多个形状、文本和其他的分组
  Shape,    // 形状定义，可填充
  Path,     // 路径
  LinearGradient,    // 渐变色
  Pattern,           // 填充图片
  ClippingRectangle, // 剪辑
} = ART;

const DeviceWidth = Dimensions.get('window').width;      //设备的宽度


export default class ARTRoundButton extends Component {
  static defaultProps = {
    style: {},
    diameter: DeviceWidth, //直径
    space: 2,

    strokeWidth: 1,
    strokeColor: '#E0E4E5',
  }

  render() {
    let diameter = this.props.diameter;
    let strokeColor = this.props.strokeColor;
    let strokeWidth = this.props.strokeWidth;

    let space = this.props.space;
    let widthHalf = diameter/4;

    let line_path = Path()
    .moveTo(widthHalf, widthHalf*2)
    .lineTo(widthHalf*2-space, diameter/2+widthHalf/2-space)
    .lineTo(widthHalf*3, widthHalf)
    .lineTo(widthHalf*2-space, diameter/2+widthHalf-space)
    .close();


    return(
      <Surface width={diameter} height={diameter} style={[{flex:1 ,backgroundColor: 'transparent'},this.props.style]}>
        <Group>
          <Shape d={line_path} stroke={strokeColor} strokeWidth={strokeWidth} fill="white"/>
        </Group>
      </Surface>
    )
  }
}

