/**
 * 
 * @project : privateNotes
 * 
 * @file: art-clippingrectangle.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 12:43:03 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 17th 2019, 12:43:03 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 * 
 *                          不可用、只是学习用的
 * 
 */


'use strict'
import React, { Component } from 'react'
import {
  Dimensions,
  ART
} from 'react-native'

var {
  Surface,  //  一个矩形可渲染的区域，是其他元素的容器
  Group,    // 可容纳多个形状、文本和其他的分组
  Shape,    // 形状定义，可填充
  Path,     // 路径
  LinearGradient,    // 渐变色
  Pattern,           // 填充图片
  ClippingRectangle, // 剪辑
} = ART;

const DeviceWidth = Dimensions.get('window').width;      //设备的宽度

export default class ARTClippingrectangle extends Component {
  static defaultProps = {
    style: {},
    width: DeviceWidth,
    height: 2,

    // dotStyle: DotStyle_default,
    strokeWidth: 0.5,
    strokeDash: [4, 2,] ,

    strokeColor: '#E0E4E5',

  }

  render() {
    // let width = this.props.width;
    // let height = this.props.height;
    // let strokeDash = this.props.strokeDash;
    // let strokeColor = this.props.strokeColor;
    // let strokeWidth = this.props.strokeWidth;
    // let dot_draw_top = (height - strokeWidth)/2

    /**
     *  绘制
     * 
     *  正常情况下  arc（）只用来画圆，想要花圆弧使用...
     */
  
    return(
      <Surface width={300} height={200} style={{backgroundColor: 'yellow', marginTop: 10}}>
        <ClippingRectangle
              width={ 100 } //要剪辑的宽
              height={ 50 } //要剪辑的高
              x={ 40 } //要剪辑的left
              y={ 0 }  //要剪辑的top
        >
          <Shape d={ new Path().moveTo(20,20).lineTo(100,20) } stroke="black" strokeWidth={50}/>
        </ClippingRectangle>
        <Shape d={ new Path().moveTo(20, 100).lineTo(100,100) } stroke="black" strokeWidth={50}/>
      </Surface>
    )
  }
}








