/**
 * 
 * @project : privateNotes
 * 
 * @file: art-semicircle.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 11:55:24 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 17th 2019, 11:55:24 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `画圆、半圆`
 * 
 * @flow
 * 
 * 
 * 
 *                            不可用、只是学习用的
 */



'use strict'
import React, { Component } from 'react'
import {
  Dimensions,
  ART
} from 'react-native'

var {
  Surface,  //  一个矩形可渲染的区域，是其他元素的容器
  Group,    // 可容纳多个形状、文本和其他的分组
  Shape,    // 形状定义，可填充
  Path,     // 路径
  LinearGradient,    // 渐变色
  Pattern,           // 填充图片
  ClippingRectangle, // 剪辑
} = ART;

const DeviceWidth = Dimensions.get('window').width;      //设备的宽度


export default class ARTSemicircle extends Component {
  static defaultProps = {
    style: {},
    width: DeviceWidth,
    height: 2,

    // dotStyle: DotStyle_default,
    strokeWidth: 0.5,
    strokeDash: [4, 2,] ,

    strokeColor: '#E0E4E5',

  }

  render() {
    let width = this.props.width;
    let height = this.props.height;
    let strokeDash = this.props.strokeDash;
    let strokeColor = this.props.strokeColor;
    let strokeWidth = this.props.strokeWidth;
    let dot_draw_top = (height - strokeWidth)/2

    /**
     *  绘制
     * 
     *  正常情况下  arc（）只用来画圆，想要花圆弧使用...
     */
    
    let circle_path = Path() //实际是由两条 圆弧 构成了一个圆（左右两半拼成一个圆）
    .moveTo(50, 0)// 起点位置
    .arc(0,100,50) // 将 (50,0) 看成新的坐标系（0,0），由此进行 顺时针（100正） 的画半弧。
    .arc(0,-100,50) // 将 (0,100) 看成新的坐标系（0,0），由此进行 逆时针（-100负） 的画半弧。
    .close() // arc第一个参数为 0 是因为 终点的x和原点在一条直线上，确保画的是半圆弧


    let circle_path_1 = Path()
    .moveTo(150,50)
    .arc(50, 100,50) // 给定了终点的坐标，即可自动算出半径,第三个参数 的范围预估 不可大于 真正的半径 不可小于1
    .close()

    let circle_path_2 = Path()
    .moveTo(50, 5)   // 起点位置
    .arc(0, 100, 50) // 将起点位置看成新的坐标系，（50, 5）为新的原点（0,0）。100为正值，顺时针方向画半圆，50 为半径，100为终点坐标
    .arc(0, 80, 40)  // 将（0,100）看成新的坐标系（0,0），80为正值，顺时针方向画半圆，40 为半径，80为终点坐标
    .arc(0, 60, 30)  // 将（0, 80）看成新的坐标系（0,0），60为正值，顺时针方向画半圆，30 为半径，60为终点坐标
    .arc(0, -20, 10) // 将（0, 60）看成新的坐标系（0,0），20为负值，逆时针方向画半圆，10 为半径，-20为终点坐标
    .close();
    return(
      <Surface width={width} height={250} style={[{flex:1 ,backgroundColor: 'transparent'},this.props.style]}>
        <Group>
          <Shape d={circle_path} stroke="#000000" strokeWidth={1} fill="#892265"/>

          <Shape d={circle_path_1} stroke="#000000" strokeWidth={1} fill="#892265"/>
          <Shape d={circle_path_2} stroke="#000000" strokeWidth={1} fill="#892265"/>
        </Group>
      </Surface>
    )
  }
}






