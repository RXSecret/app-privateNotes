/**
 * 
 * @project : privateNotes
 * 
 * @file: art-menu.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Friday, April 19th 2019, 3:26:37 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Friday, April 19th 2019, 3:26:37 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: ` 画 --> 三 `
 * 
 * @flow
 * 
 */




'use strict'
import React, { Component } from 'react'
import {
  Dimensions,
  ART
} from 'react-native'

var {
  Surface,  //  一个矩形可渲染的区域，是其他元素的容器
  Group,    // 可容纳多个形状、文本和其他的分组
  Shape,    // 形状定义，可填充
  Path,     // 路径
  LinearGradient,    // 渐变色
  Pattern,           // 填充图片
  ClippingRectangle, // 剪辑
} = ART;

const DeviceWidth = Dimensions.get('window').width;      //设备的宽度


export default class ARTDottedLine extends Component {
  static defaultProps = {
    style: {},
    width: DeviceWidth,
    height: 2,

    strokeWidth: 1.5,
    strokeColor: 'white',
  }

  render() {
    let width = this.props.width;
    let height = this.props.height;

    let strokeColor = this.props.strokeColor;
    let strokeWidth = this.props.strokeWidth;
    let top = strokeWidth/2;
    let dot_draw_top = (height - strokeWidth)/2+top

    let last_dot_top = height - top;

    /*绘制*/
    let dot_path_1 = Path()
    .moveTo(0, top) // 改变起点为 x,x 。默认为0,0
    .lineTo(width, top) // 目标点

    let dot_path_2 = Path()
    .moveTo(0, dot_draw_top) 
    .lineTo(width, dot_draw_top) 

    let dot_path_3 = Path()
    .moveTo(0, last_dot_top) 
    .lineTo(width, last_dot_top)
  
    return(
      <Surface width={width} height={height} style={[{flex:1 ,backgroundColor: 'transparent'},this.props.style]}>
        <Group>
          <Shape d={dot_path_1} stroke={strokeColor} strokeWidth={strokeWidth} />
          <Shape d={dot_path_2} stroke={strokeColor} strokeWidth={strokeWidth} />
          <Shape d={dot_path_3} stroke={strokeColor} strokeWidth={strokeWidth} />
        </Group>
      </Surface>
    )
  }
}

