/**
 * 
 * @project : privateNotes
 * 
 * @file: art-linear-gradient.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 12:47:58 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 17th 2019, 12:47:58 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 * 
 * 
 *                          不可用、只是学习用的
 *     https://www.jianshu.com/p/f6f57c3406ea
 */


'use strict'
import React, { Component } from 'react'
import {
  Dimensions,
  ART,
  View
} from 'react-native'

var {
  Surface,  //  一个矩形可渲染的区域，是其他元素的容器
  Group,    // 可容纳多个形状、文本和其他的分组
  Shape,    // 形状定义，可填充
  Path,     // 路径
  Text,
  LinearGradient,    // 渐变色
  Pattern,           // 填充图片
  ClippingRectangle, // 剪辑
} = ART;

const DeviceWidth = Dimensions.get('window').width;      //设备的宽度

export default class ARTLinearGradient extends Component {
  static defaultProps = {
    style: {},
    width: DeviceWidth,
    height: 2,

    // dotStyle: DotStyle_default,
    strokeWidth: 0.5,
    strokeDash: [4, 2,] ,

    strokeColor: '#E0E4E5',

  }

  render() {
  

    // 颜色占比表示 50%，  那个颜色的占比大，在前；颜色比小，在后。不能大于1
    // 同样的占比，后面的会把前面的替换掉
    // 必须是 Int 类型， 不可以是float或其他

    // x1,y1,x2,y2   x1,y1颜色的起点；x2,y2颜色的终点
    // "0", "0", "300", "0"    300为组件的宽度、弧度的长度   此：表示 横向颜色渐变
    // "0", "0", "0", "40"     40为组件的高度、弧度的高度   此：表示 纵向颜色渐变
    // "0", "0", "300", "100"  300、100 渐变是横向 ///(渐变样式犹如斜杠) 渐变

    // var linearGradient = new LinearGradient({
    //         '.4': 'green',
    //         '.6': 'blue',
    //     },
    //     "0", "0", "300", "100"
    // );

    let path_1 = new Path()
    .moveTo(40,40)
    .lineTo(99,10)

    return(
      <View style={{flex: 1}}>
        <Surface width={100} height={100}>
            {/* <Text strokeWidth={1} stroke="#000" path={path_1} >Swipe</Text> */}
            <Text strokeWidth={1} stroke="#000" font="bold 35px Heiti SC" path={new Path().moveTo(40,40).lineTo(99,10)} >Swipe</Text>
        </Surface>
    </View>
    )
  }
}

/**
 * 
 * 
 * <Surface width={300} height={300} style={{backgroundColor: 'yellow', marginTop: 10}}>
        <Wedge
              outerRadius={100}
              innerRadius={90}
              startAngle={0}
              endAngle={100}
              originX={80}
              originY={50}
              fill={linearGradient}/>
        </Surface>

        <Surface width={300} height={100} style={{backgroundColor: 'yellow', marginTop: 10}}>
          <Shape d={juxing_jianbian_path} stroke="#000000" strokeWidth={1} fill={linearGradient}/>
        </Surface> 
 */











