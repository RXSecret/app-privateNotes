/**
 * 
 * @project : privateNotes
 * 
 * @file: art-dottedLine.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 17th 2019, 11:19:57 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 17th 2019, 11:19:57 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `虚线`
 * 
 * @flow
 * 
 * 
 * @Example usage(e.g):
 * 
 * <ARTDottedLine
 *  //----
 *  style={ {flex: 1, marginTop: 5} }
 *  width={6} //default device-width
 *  height={2} //default 2
 *  //----
 *  strokeWidth={0.5} 
 *  strokeDash={ [4,2,] } // [5, 10, 20, 5]
 *  strokeColor={ 'red'}  // rgba rgb ...
 * />
 * 
 */


'use strict'
import React, { Component } from 'react'
import {
  Dimensions,
  ART
} from 'react-native'

var {
  Surface,  //  一个矩形可渲染的区域，是其他元素的容器
  Group,    // 可容纳多个形状、文本和其他的分组
  Shape,    // 形状定义，可填充
  Path,     // 路径
  LinearGradient,    // 渐变色
  Pattern,           // 填充图片
  ClippingRectangle, // 剪辑
} = ART;

const DeviceWidth = Dimensions.get('window').width;      //设备的宽度
// const DeviceHeight = Dimensions.get('window').height;    //设备的高度

// const DotStyle_default = 'default'
// const DotStyle_default = 'vertical'
// const DotStyle_default = 'left_top_right_bottom' 
// const DotStyle_default = 'right_top_left_bottom'

export default class ARTDottedLine extends Component {
  static defaultProps = {
    style: {},
    width: DeviceWidth,
    height: 2,

    // dotStyle: DotStyle_default,
    strokeWidth: 0.5,
    strokeDash: [4, 2,] ,

    strokeColor: '#E0E4E5',

  }

  render() {
    let width = this.props.width;
    let height = this.props.height;
    let strokeDash = this.props.strokeDash;
    let strokeColor = this.props.strokeColor;
    let strokeWidth = this.props.strokeWidth;
    let dot_draw_top = (height - strokeWidth)/2

    /*绘制 虚线*/
    let dot_path = Path()
    .moveTo(0, dot_draw_top) // 改变起点为 x,x 。默认为0,0
    .lineTo(width,dot_draw_top) // 目标点
    
    // e.g
    // let dot_path_1 = Path()
    // .moveTo(0,15) // 改变起点为 0,15 。默认为0,0
    // .lineTo(width,15) // 目标点

    return(
      <Surface width={width} height={height} style={[{flex:1 ,backgroundColor: 'transparent'},this.props.style]}>
        <Group>
          <Shape d={dot_path} stroke={strokeColor} strokeWidth={strokeWidth} strokeDash={strokeDash}/>

          {/* e.g */}
          {/*strokeDash={[5,20]  先是5像素的实线，再是20像素的空白，再是5像素的实线... 循环*/}
          {/* <Shape d={xuxian_path} stroke="#000000" strokeWidth={2} strokeDash={[5,20,]}/> */}

          {/*strokeDash={[10,5,20,5]  先是10像素的实线，再是5像素的空白，再是20像素的实线, 再是5像素的空白... 循环*/}
          {/* <Shape d={dot_path_1} stroke="#000000" strokeWidth={2} strokeDash={[10,5,20,5]}/> */}
        </Group>
      </Surface>
    )
  }
}





