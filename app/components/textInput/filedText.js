/**
 * 
 * @project : privateNotes
 * 
 * @file: filedText.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Saturday, April 20th 2019, 12:13:52 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Saturday, April 20th 2019, 12:13:52 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 * 这里得到缓存、存储功能
 * 
 */

'use strict'

import React, {Component}  from 'React'
import {StyleSheet} from 'react-native'

import InputView from './input'

export default class FiledText extends Component {
  static defaultProps = {
    style: {},
    defaultValue: '',
    placeholder:'',
    placeholderColor: '#ccc',
    multiline: true,
    blurOnSubmit: false,
    returnKeyType:'next',
  }

  constructor(props) {
    super(props);
    this.state = ({

    })
  }

  componentWillUnmount() {
    if(this.refTextInput) {
      this.refTextInput.blur();
    }
  }

  blur() {
    if(this.refTextInput) {
        this.refTextInput.blur();
    }
  }

  focus() {
    if(this.refTextInput) {
        this.refTextInput.focus();
    }
  }

  render(){
    return(
      <InputView 
        {...this.props} 
        ref={refTextInput=>this.refTextInput=refTextInput}
      />
    )
  }

}




