/**
 * 
 * @project : privateNotes
 * 
 * @file: input.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Friday, April 19th 2019, 10:44:55 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Friday, April 19th 2019, 10:44:55 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

'use strict'

import React, {Component}  from 'React'

import {StyleSheet, TextInput} from 'react-native'

export default class InputView extends Component {

  static defaultProps = {
    style: {},
    defaultValue: '',
    placeholder:'',
    placeholderColor: '#ccc',
    multiline: false,
    selectTextOnFocus: false,
    autoFocus: false,
    returnKeyType:'done',
    blurOnSubmit: true,
    maxLength: 1000,
  }

  constructor(props) {
    super(props);
    this.state = ({

    })
  }

  componentWillUnmount() {
    this.blur;
  }

  blur() {
    if(this.refTextInput) {
        this.refTextInput.blur();
    }
  }

  focus() {
    if(this.refTextInput) {
        this.refTextInput.focus();
    }
  }



  render(){
    return(
      <TextInput 
        style={[styles.container, this.props.style]}
        ref={refTextInput=>this.refTextInput=refTextInput}
        defaultValue={this.props.defaultValue}  /*  提供一个文本框中的初始值。当用户开始输入的时候，值就可以改变。
                                  *  在一些简单的使用情形下，如果你不想用监听消息然后更新value属性的方法来保持属性
                                  *  和状态同步的时候，就可以用defaultValue来代替。
                                  */
                              
        placeholder={this.props.placeholder} //文本框的默认显示
        placeholderTextColor={this.props.placeholderColor} //文本框的默认显示字的颜色
        selectTextOnFocus ={false}  //如果为true，当获得焦点的时候，所有的文字都会被选中。
        // selectionColor={'bule'}  //设置输入框高亮时的颜色（在iOS上还包括光标）
        multiline={this.props.multiline}     //多行输入 如果为true
        autoCorrect={false}  //如果为false，会关闭拼写自动修正。默认值是true。
        autoFocus={this.props.autoFocus}     //自动获取焦点
        returnKeyType={this.props.returnKeyType}  /* done go next search send */
        // returnKeyLabel={'完成'}
        blurOnSubmit={this.props.blurOnSubmit} /* 
                             * 如果为true，文本框会在提交的时候失焦。对于单行输入框默认值为true，
                             * 多行则为false。注意：对于多行输入框来说，如果将blurOnSubmit设为true，
                             * 则在按下回车键时就会失去焦点同时触发onSubmitEditing事件，而不会换行。
                             */
        autoCapitalize={"none"} /* 
                               * 控制TextInput是否要自动将特定字符切换为大写：可选的值为：
                               * characters: 所有的字符。
                               * words: 每个单词的第一个字符.
                               * sentences: 每句话的第一个字符（默认）。
                               * none: 不自动切换任何字符为大写。
                               */

        maxLength={this.props.maxLength}    //限制文本框中最多的字符数。使用这个属性而不用JS逻辑去实现，可以避免闪烁的现象。
        editable={true}     //如果为false，文本框是不可编辑的。默认值为true。
        dataDetectorTypes={'all'} /** 'phoneNumber' 'link' 'address' 'calendarEvent' 'none' 'all' */
        // contextMenuHidden={'false'} /** bool, false */

        // keyboardType={'numeric'} 
                                  /* 
                                  * 决定弹出的何种软键盘的  这些值在所有平台都可用：
                                  * default 默认的键盘  
                                  * numeric  数字  
                                  * email-address 电子邮箱地址
                                  */
        underlineColorAndroid={'transparent'} 
                                    /*TextInput在安卓上默认有一个底边框，同时会有一些padding。
                                      如果要想使其看起来和iOS上尽量一致，则需要设置padding: 0，
                                      同时设置underlineColorAndroid="transparent"来去掉底边框。
                                      而且，在安卓上如果设置multiline = {true}，文本默认会垂直居中，
                                      可设置textAlignVertical: top样式来使其居顶显示。
                                      */ 
        onFocus={()=>{
          if(this.props.onFocus) {
            this.props.onFocus();
          }
        }}

        onChangeText={(text) => { //当文本框内容变化时调用此回调函数。
          if(this.props.onChangeText){
            this.props.onChangeText(text)
          }
        }}  

        onBlur ={()=>{
          if(this.props.onBlur) {
            this.props.onBlur();
          }
        }}

        onLayout={(event)=>{
          if(this.props.onLayout) {
            this.props.onLayout(event);
          }
        }}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
      margin: 0,
      padding: 0,
      textAlignVertical: 'top',
      // color:'red',
      color: '#666',
  },
  
})



