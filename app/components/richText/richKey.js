/**
 * 
 * @project : privateNotes
 * 
 * @file: richKey.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Saturday, April 20th 2019, 10:44:05 am
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Saturday, April 20th 2019, 10:44:05 am
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 */

const RichKey = {

  width: 'rt_text_width',
  height: 'rt_text_height',
  backgrounColor: 'rt_text_backgrounColor',

  margin: 'rt_text_margin',
  marginLeft : 'rt_text_marginLeft',
  marginTop : 'rt_text_marginTop',
  marginRight : 'rt_text_marginRight',
  marginBottom : 'rt_text_marginBottom',

  padding: 'rt_text_padding',
  paddingLeft: 'rt_text_padding',
  paddingTop: 'rt_text_padding',
  paddingRight: 'rt_text_paddingRight',
  paddingBottom: 'rt_text_paddingBottom',

  color : 'rt_text_color',
  fontSize: 'rt_text_fontSize',
  fontWeight: 'rt_text_fontWeight',
  lineHeight: 'rt_text_lineHeight',
  fontFamily: 'rt_text_fontFamily',
  fontStyle: 'rt_text_fontStyle',
  textShadowColor: 'rt_text_textShadowColor',

  imgLoc : 'rt_text_img_loc',
  imgUri : 'rt_text_img_uri',

  emoji: 'rt_text_emoji',

  emojiDIY: 'rt_text_emojiDIY',
}
export default RichKey;