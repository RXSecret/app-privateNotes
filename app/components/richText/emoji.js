/**
 * 
 * @project : privateNotes
 * 
 * @file: emoji.js
 * 
 * @author: appPro-01
 * 
 * @created_date : Wednesday, April 24th 2019, 7:13:16 pm
 * --------------------------------------------------
 * @last_modified : appPro-01
 * @modified_by : Wednesday, April 24th 2019, 7:13:16 pm
 * 
 * --------------------------------------------------
 * 
 * MIT License
 * 
 * Copyright (c) 2019 appPro
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * --------------------------------------------------
 * 
 * @fileoverview: `function describe / 功能描述`
 * 
 * @flow
 * 
 * apple支持哪些emoji
 * mac上 control+command+space 打开emoji 点击拷贝简介 可以看多所有apple所支持的
 * 
 * 表情网站:
 * UTF-16 最新请看 http://www.iemoji.com/ (Emoji Character Encoding Data->C/C++/Java Src) 
 * 
 */

export default Emoji = [
  '\uD83D\uDE00', //😀 
  '\uD83D\uDE03', //😃 
  '\uD83D\uDE04', //😄
  '\uD83D\uDE01', //😁
  '\uD83D\uDE06', //😆
  '\uD83D\uDE05', //😅
  '\uD83D\uDE02', //😂
  // '\uD83E\uDD23', //🤣 win
  // '\u263A\uFE0F', //☺️ win mac
  '\uD83D\uDE0A', //😊
  '\uD83D\uDE07', //😇
  '\uD83D\uDE42', //🙂
  // '\uD83D\uDE43', //🙃 win
  '\uD83D\uDE09', //😉
  '\uD83D\uDE0C', //😌
  '\uD83D\uDE0D', //😍
  // '\uD83E\uDD70', //🥰 win mac
  '\uD83D\uDE18', //😘
  '\uD83D\uDE17', //😗
  '\uD83D\uDE19', //😙
  '\uD83D\uDE1A', //😚
  '\uD83D\uDE0B', //😋
  '\uD83D\uDE1B', //😛
  '\uD83D\uDE1D', //😝
  '\uD83D\uDE1C', //😜
  // '\uD83E\uDD2A', //🤪 win
  // '\uD83E\uDD28', //🤨 win
  // '\uD83E\uDDD0', //🧐 win
  // '\uD83E\uDD13', //🤓 win
  '\uD83D\uDE0E', //😎
  // '\uD83E\uDD29', //🤩 win
  // '\uD83E\uDD73', //🥳 win mac
  '\uD83D\uDE0F', //😏
  '\uD83D\uDE12', //😒
  '\uD83D\uDE1E', //😞
  '\uD83D\uDE14', //😔
  '\uD83D\uDE1F', //😟
  '\uD83D\uDE15', //😕
  '\uD83D\uDE41', //🙁
  '\u2639\uFE0F', //☹️
  '\uD83D\uDE23', //😣
  '\uD83D\uDE16', //😖
  '\uD83D\uDE2B', //😫
  '\uD83D\uDE29', //😩
  // '\uD83E\uDD7A', //🥺 win mac
  '\uD83D\uDE22', //😢
  '\uD83D\uDE2D', //😭
  '\uD83D\uDE24', //😤
  '\uD83D\uDE20', //😠
  '\uD83D\uDE21', //😡
  // '\uD83E\uDD2C', //🤬 win
  // '\uD83E\uDD2F', //🤯 win
  '\uD83D\uDE33', //😳
  // '\uD83E\uDD75', //🥵 win mac
  // '\uD83E\uDD76', //🥶 win mac
  '\uD83D\uDE31', //😱
  '\uD83D\uDE28', //😨
  '\uD83D\uDE30', //😰
  '\uD83D\uDE25', //😥
  '\uD83D\uDE13', //😓
  // '\uD83E\uDD17', //🤗 win
  // '\uD83E\uDD14', //🤔 win
  // '\uD83E\uDD2D', //🤭 win
  // '\uD83E\uDD2B', //🤫 win
  // '\uD83E\uDD25', //🤥 win
  '\uD83D\uDE36', //😶
  '\uD83D\uDE10', //😐
  '\uD83D\uDE11', //😑
  '\uD83D\uDE2C', //😬
  // '\uD83D\uDE44', //🙄 win
  '\uD83D\uDE2F', //😯
  '\uD83D\uDE26', //😦
  '\uD83D\uDE27', //😧
  '\uD83D\uDE2E', //😮
  '\uD83D\uDE32', //😲
  '\uD83D\uDE34', //😴
  // '\uD83E\uDD24', //🤤 win
  '\uD83D\uDE2A', //😪
  '\uD83D\uDE35', //😵
  // '\uD83E\uDD10', //🤐 win
  // '\uD83E\uDD74', //🥴 win mac
  // '\uD83E\uDD22', //🤢 win
  // '\uD83E\uDD2E', //🤮 win
  // '\uD83E\uDD27', //🤧 win
  '\uD83D\uDE37', //😷
  // '\uD83E\uDD12', //🤒 win
  // '\uD83E\uDD15', //🤕 win
  // '\uD83E\uDD11', //🤑 win
  // '\uD83E\uDD20', //🤠 win
  '\uD83D\uDE08', //😈
  '\uD83D\uDC7F', //👿
  '\uD83D\uDC79', //👹
  '\uD83D\uDC7A', //👺
  // '\uD83E\uDD21', //🤡 win
  '\uD83D\uDCA9', //💩
  '\uD83D\uDC7B', //👻
  
  ''
]


