# app-privateNotes

#### 介绍
私人笔记

<br />

[*  *创作思路*  *](https://gitee.com/RXSecret/app-privateNotes/blob/master/app)

<br />

#### VSCode 快捷键 (扩展库)
Mac keys:
<br />
`psioniq File Header`      ：  control+option+h * 2
<br />
`markdown Preview Enhanced` : 

<br />
<br />

Windows keys:
<br />
`psioniq File Header`       ：  F1
<br />
`markdown Preview Enhanced` :   ctrl + shift + V
<br />

---

<br /><br />

### 想看看 `React-native` 项目?

#### 1、环境搭建 `React-native` 环境

(1)、`macOS` 安装依赖有： Node、Watchman。
```sh
# 推荐用 `Homebrew`安装
brew install node
brew install watchman

# 安装RN
# 在工程根目录 `终端` 运行 
npm install
```


<br />

(2)、`windows` 安装依赖有： Node、Python2、Java SDK、Android、Android studio。
<br />
自行去官网下载 xx.exe 进行安装
<br /><br />


#### 2、Mac - iOS 配置 + 第三方库
进入`ios`目录，运行 `pod install`

<br />

#### 3、windows - Android studio 配置
1、下载最新的gradle 、gradle tool 、以及推荐安装的库
<br />
2、`项目中的gradle`和`Android Studio的 gradle`版本不一致，如果下载列表中，有就下载；没有 就下载最新的，需要重新配置项目中的gradle设置
> 如需设置：
- android`->`gradle`->`wrapper`->`gradle-wrapper.properties
- android`->`build.gradle
<br /><br />


#### 4、最后一步了
1、在工程根目录 `终端` 运行 `npm start`
<br />
2、启动 原生(`iOS`/`Android`)项目

<br /><br />

---

<br /><br />

#### 打包
1、react-native 打包
```sh
# Mac 
npm run ios/android

# windows
npm run win_ios/android
```
2、原生(`iOS`/`Android`)项目打包
<br />
暂不支持脚本打包
<br />
请在原生中打包